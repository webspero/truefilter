import React from 'react';

const Header = (props) => {

    return (
        <>
        <header id="header" className="header fixed-top">
            <div className="container-fluid container-xl d-flex align-items-center justify-content-between">

                <a href="index.html" className="logo d-flex align-items-center">
                    <img src="assets/img/logo.png" alt="" />
                    <span>true</span>
                </a>

                <nav id="navbar" className="navbar">
                    <ul>
                        <li><a className="nav-link scrollto active" href="#hero">{props.data.work}</a></li>
                        <li><a className="nav-link scrollto" href="#goals">{props.data.goals}</a></li>
                        <li><a className="nav-link scrollto" href="#help">{props.data.help}</a></li>
                        <li><a className="getstarted scrollto" href="#Sign">{props.data.signIn}</a></li>
                    </ul>
                    <i className="bi bi-list mobile-nav-toggle"></i>
                </nav>

            </div>
        </header>
        </>
    )
}

export default Header;