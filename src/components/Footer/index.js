import React from "react";

const Footer = (props) => {

    return (
        <footer>
        <div class="container">
            <div class="footer-cont">
                <div class="footer-logo">
                    <img alt="" src="assets/image/tf-logo.svg" />
                </div>
                <div class="footer-nav">
                    <ul>
                        <li><a href="./">{props.data.notice}</a> </li>
                        <li><a href="./">{props.data.privacy}</a> </li>
                        <li><a href="./">{props.data.terms}</a> </li>
                    </ul>
                </div>
            </div>
            <div class="footer-copyright">
                <p>{props.data.footer}</p>
            </div>
        </div>
    </footer>
    )
}

export default Footer;