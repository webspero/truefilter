import React, { useState } from "react";

const Content = (props) => {

    const [typedValue, setTypedValue] = useState('');
    const [displayTypedValue, setDisplayTypedValue] = useState('')

    const handleChange = (event) => {
        setTypedValue(event.target.value)
    }

    const handleClick = () => {
        setDisplayTypedValue(typedValue)
    }

    return (
        <>
        <section id="hero-bg" className="">
            <div id="hero-section">
                <div className="container">
                    <div className="hero-sec-block">
                        <h1>{props.data.contentHeading}</h1>
                        <p>{props.data.aiData}</p>
                        <ul className="">
                            <li><a href="./"><img alt="" src="assets/image/twitter.svg" /> </a> </li>
                            <li><a href="./"><img alt="" src="assets/image/slack.svg" /> </a> </li>
                            <li><a href="./"><img alt="" src="assets/image/youtube.svg" /> </a> </li>
                            <li><a href="./"><img alt="" src="assets/image/instagram.svg" /> </a> </li>
                        </ul>
                        <p className="try">{props.data.solutions} <span>{props.data.solutionUnderstanding}</span></p>
                    </div>
                </div>
            </div>
        </section>
        <section className="formtextarea">
            <div className="container">
                <form>
                    <div className="textarea-box">
                        <textarea name="message" placeholder={props.data.textareaplaceholder} onChange={handleChange} className="textarea"></textarea>
                        <div className="text-list">
                            <ul>
                                <li><a href="./">{props.data.point1}</a></li>
                                <li><a href="./">{props.data.point2}</a></li>
                                <li><a href="./">{props.data.point3}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="test-btn">
                        <button onClick={handleClick} type="button">{props.data.message}</button>
                    </div>
                </form>
            </div>
            <div className="typed-value">{displayTypedValue}</div>
        </section>
        </>
    )
}

export default Content;