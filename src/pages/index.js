import Content from "../components/Content";
import Footer from "../components/Footer";
import Header from "../components/Header";
import data from "../data";

const MainApp = () => {
    return (
        <>
        <Header data={data.Header}/>
        <Content data={data.Content} />
        <Footer data={data.Footer} />
        </>
    )
}

export default MainApp;