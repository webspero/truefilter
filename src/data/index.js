const data = {
    Header: {
      work: "How it works",
      goals: "Our goals",
      help: "How to help",
      signIn: "Sign in",
    },
    Content: { 
      contentHeading: "Clean up your space from unwanted intrusive messages",
      aiData: "using our world-className AI-based data filter for",
      solutions: "Try our solution now",
      solutionUnderstanding: "to understand how it works",
      point1: "Classic internet bullying",
      point2: "Bodyshaming a jellyfish",
      point3: "Super negative feedback",
      message: "Test message",
      textareaplaceholder: "Enter your text here or select one of the examples"
    },
    Footer: { 
      notice: "Legal Notice",
      privacy: "Privacy",
      terms: "Terms",
      footer: "© 2022 True Global Ltd. All rights reserved.",
    }
  };

export default data;
